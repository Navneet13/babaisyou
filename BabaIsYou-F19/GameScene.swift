//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    // MARK: ENUM
    enum WallRule {
        case WALL_WIN
        case WALL_STOP
    }
    
    enum FlagRule {
        case FLAG_WIN
        case FLAG_STOP
    }
    
    var wallRuleActivated: WallRule? = .WALL_STOP
    var flagRuleActivated: FlagRule? = .FLAG_WIN
    
    //MARK: Sprites
    var baba: SKSpriteNode!
    var wall: SKSpriteNode!
    var flag: SKSpriteNode!
    
    var wallBlock: SKSpriteNode!
    var flagBlock: SKSpriteNode!
    var stopBlock : SKSpriteNode!
    var winBlock: SKSpriteNode!
    var isFlagBlock: SKSpriteNode!
    var isWallBlock: SKSpriteNode!
    
    var isWallActivated = false
    
    let PLAYER_SPEED: CGFloat = 40
    
    // MARK: DID_MOVE
    override func didMove(to view: SKView) {
        
        self.physicsWorld.contactDelegate = self
        
        // Initializing the Sprites
        self.baba = self.childNode(withName: "baba") as? SKSpriteNode
        self.wall = self.childNode(withName: "wall") as? SKSpriteNode
        self.flag = self.childNode(withName: "flag") as? SKSpriteNode
        self.wallBlock = self.childNode(withName: "wallBlock") as? SKSpriteNode
        self.flagBlock = self.childNode(withName: "flagBlock") as? SKSpriteNode
        self.stopBlock = self.childNode(withName: "stopBlock") as? SKSpriteNode
        self.winBlock = self.childNode(withName: "winBlock") as? SKSpriteNode
        self.isFlagBlock = self.childNode(withName: "isFlagBlock") as? SKSpriteNode
        self.isWallBlock = self.childNode(withName: "isWallBlock") as? SKSpriteNode
        
        //        for node in [baba, wall, flag] {
        //            node?.physicsBody?.collisionBitMask = 0 //  collides with nothing
        //        }
        //
        //        self.baba.physicsBody?.categoryBitMask = babaCategory
        //        self.wall.physicsBody?.categoryBitMask = wallCategory
        //         self.flag.physicsBody?.categoryBitMask = flagCategory
        //        self.wall.physicsBody?.collisionBitMask = babaCategory
        //        self.flag.physicsBody?.collisionBitMask = babaCategory
        //
        //        self.wall.physicsBody?.contactTestBitMask = self.wall.physicsBody?.collisionBitMask ?? 0
        //        self.flag.physicsBody?.contactTestBitMask = self.flag.physicsBody?.collisionBitMask ?? 0
        //        self.baba.physicsBody?.contactTestBitMask = self.baba.physicsBody?.collisionBitMask ?? 0
        
        
        //        self.wallBlock.physicsBody?.categoryBitMask = 64
        //
        
        //        self.flagBlock.physicsBody?.categoryBitMask = 128
        
        
        //        self.stopBlock.physicsBody?.categoryBitMask = 256
        
        
        
        //        self.winBlock.physicsBody?.categoryBitMask = 1
        
        
        //        self.isBlock.physicsBody?.categoryBitMask = 32
    }
    
    // MARK: DID_BEGIN
    func didBegin(_ contact: SKPhysicsContact) {
        
        checkRule()
        
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        
        if nodeA == nil || nodeB == nil{
            return
        }
        
        // MARK: BABA-WALL COLLIDE
        // IF NODE A is WALL AND NODE B is BABA
        // OR
        // IF NODE A is BABA AND NODE B is WALL
        if (nodeA!.name == "baba" && nodeB!.name == "wall") || (nodeA!.name == "wall" && nodeB!.name == "baba") {
            switch wallRuleActivated {
            case .WALL_WIN:
                showMessageAlert(message: "You have won the game")
                break
            case .WALL_STOP:
                print("WALL AND BABA COLLIDED AND STOP")
                break
            case .none:
                print("Move the baba over wall")
                break
            }
        }
        
        // MARK: BABA-FLAG COLLIDE
        // IF NODE A is FLAG AND NODE B is BABA
        // OR
        // IF NODE A is BABA AND NODE B is FLAG
        if (nodeA!.name == "baba" && nodeB!.name == "flag") || (nodeA!.name == "flag" && nodeB!.name == "baba") {
             // print("FLAG AND BABA COLLIDED")
            switch flagRuleActivated {
            case .FLAG_WIN:
                showMessageAlert(message: "You have won the game")
                break
            case .FLAG_STOP:
                print("FLAG AND BABA COLLIDED AND STOP")
                break
            case .none:
                print("Move the baba over flag")
                break
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    // MARK: TOUCHES MOVE
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let mouseTouch = touches.first
        if (mouseTouch == nil) {
            return
        }
        
        let location = mouseTouch!.location(in: self)
        let nodeTouched = atPoint(location).name
        
        checkRule()
        changeWallCollisionMasks()
        
        if(nodeTouched == "moveUp" ){
            moveUp()
        }
        if nodeTouched == "moveDown"{
            self.baba.position.y = self.baba.position.y - PLAYER_SPEED
        }
        if nodeTouched == "moveLeft"{
            self.baba.position.x = self.baba.position.x - PLAYER_SPEED
        }
        if nodeTouched == "moveRight"{
            self.baba.position.x = self.baba.position.x + PLAYER_SPEED
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    func moveUp(){
        self.baba.position.y = self.baba.position.y + PLAYER_SPEED
    }
    
    func checkRule() {
        
        if self.wallBlock.position.y == self.isWallBlock.position.y {
            
            if self.isWallBlock.position.y == self.stopBlock.position.y  {
                if isWallXAligned(rule: .WALL_STOP) {
                    wallRuleActivated = .WALL_STOP
                } else {
                    wallRuleActivated = nil
                }
            } else if self.isWallBlock.position.y == self.winBlock.position.y {
                if isWallXAligned(rule: .WALL_WIN) {
                    wallRuleActivated = .WALL_WIN
                } else {
                    wallRuleActivated = nil
                }
            } else {
                wallRuleActivated = nil
            }
        }
        
        if self.flagBlock.position.y == self.isFlagBlock.position.y {
            
            if self.isFlagBlock.position.y == self.stopBlock.position.y  {
                if isFlagXAligned(rule: .FLAG_STOP) {
                    flagRuleActivated = .FLAG_STOP
                } else {
                    flagRuleActivated = nil
                }
            } else if self.isFlagBlock.position.y == self.winBlock.position.y {
                if isFlagXAligned(rule: .FLAG_WIN) {
                    flagRuleActivated = .FLAG_WIN
                } else {
                    flagRuleActivated = nil
                }
            } else {
                flagRuleActivated = nil
            }
        }
    }
    
    private func isWallXAligned(rule: WallRule) -> Bool {
           
           let wallBlockXPosition = self.wallBlock?.position.x ?? 0.0
           let wallIsBlockPosition = self.isWallBlock?.position.x ?? 0.0
           if rule == .WALL_STOP {
               let stopBlockPosition = self.stopBlock?.position.x ?? 0.0
               
               if wallBlockXPosition < wallIsBlockPosition {
                   if wallIsBlockPosition < stopBlockPosition {
                       return true
                   }
               }
           } else if rule == .WALL_WIN {
               let winBlockPosition = self.winBlock?.position.x ?? 0.0
               
               if wallBlockXPosition < wallIsBlockPosition {
                   if wallIsBlockPosition < winBlockPosition {
                       return true
                   }
               }
           }
           return false
       }
       
       private func isFlagXAligned(rule: FlagRule) -> Bool {
           
           let flagBlockXPosition = self.flagBlock?.position.x ?? 0.0
           let flagIsBlockPosition = self.isFlagBlock?.position.x ?? 0.0
           if rule == .FLAG_STOP {
               let stopBlockPosition = self.stopBlock?.position.x ?? 0.0
               
               if flagBlockXPosition < flagIsBlockPosition {
                   if flagIsBlockPosition < stopBlockPosition {
                       return true
                   }
               }
           } else if rule == .FLAG_WIN {
               let winBlockPosition = self.winBlock?.position.x ?? 0.0
               
               if flagBlockXPosition < flagIsBlockPosition {
                   if flagIsBlockPosition < winBlockPosition {
                       return true
                   }
               }
           }
           return false
       }
    
    private func changeWallCollisionMasks() {
        
        if wallRuleActivated == .WALL_WIN ||  wallRuleActivated == .WALL_STOP {
            self.wall.physicsBody?.collisionBitMask = 1
        } else {
            self.wall.physicsBody?.collisionBitMask = 0
            self.baba.physicsBody?.collisionBitMask = 0
        }
    }
    
    private func showMessageAlert(message: String) {
        
        let controller = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        controller.addAction(okAction)
        
        if let vc = self.scene?.view?.window?.rootViewController {
            vc.present(controller, animated: true, completion: nil)
        }
    }
}
